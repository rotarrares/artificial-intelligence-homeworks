import math as m


class Particle:
    def __init__(self):
        self.__positionX = -1.37
        self.__positionY = -1.37
        self.__velocityX = 1
        self.__velocityY = 0
        self.__fitness = 0
        self.__bestPositionX = 0
        self.__bestPositionY = 0
        self.__bestFitness = 0

    def update(self, particle):
        None

    def evaluate(self):
        sExp = m.sqrt(m.pow(self.__positionX, 2) + m.pow(self.__positionY, 2))/m.pi
        fExp = m.sin(self.__positionX) * m.sin(self.__positionY) * m.exp(m.fabs(100-sExp))
        functionResult = (-0.0001) * (m.pow((m.fabs(fExp)+1), 0.1))
        self.__fitness = -(0+functionResult)
        return self.__fitness

    def getPos(self):
        return [self.__positionX,self.__positionY]

    def getBestPos(self):
        return [self.__bestPositionX,self.__bestPositionY]

    def getFit(self):
        return self.__fitness

    def getBestFit(self):
        return self.__bestFitness

